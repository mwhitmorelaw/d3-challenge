var svgWidth = 960;
var svgHeight = 500;

var margin = {
  top: 20,
  right: 40,
  bottom: 80,
  left: 100
};

var width = svgWidth - margin.left - margin.right;
var height = svgHeight - margin.top - margin.bottom;

// Create an SVG wrapper, append an SVG group that will hold our chart,
// and shift the latter by left and top margins.
var svg = d3
  .select(".chart")
  .append("svg")
  .attr("width", svgWidth)
  .attr("height", svgHeight + 200);

// Append an SVG group
var chartGroup = svg.append("g")
  .attr("transform", `translate(${margin.left}, ${margin.top})`);

// Initial Params
var chosenXAxis = "poverty";
var chosenYAxis = "obesity";

var chosenXAxisLabel = "Poverty";
var chosenYAxisLabel = "Obesity";

// function used for updating x-scale var upon click on axis label
function xScale(data, chosenXAxis) {
  // create scales
  var xLinearScale = d3.scaleLinear()
    .domain([d3.min(data, d => d[chosenXAxis]) * 0.3,
      d3.max(data, d => d[chosenXAxis]) * 2.5 
    ])
    .range([0, width]);

  return xLinearScale;
}

// function used for updating xAxis var upon click on axis label
function renderAxes(newXScale, xAxis) {
  var bottomAxis = d3.axisBottom(newXScale);

  xAxis.transition()
    .duration(1000)
    .call(bottomAxis);

  return xAxis;
}

// function used for updating circles group with a transition to
// new circles
function renderCircles(circlesGroup, newXScale, chosenXAxis, textGroup) {

  circlesGroup.transition()
    .duration(1000)
    .attr("cx", d => newXScale(d[chosenXAxis]));
  textGroup.transition()
    .duration(1000)
    .attr("x", d => newXScale(d[chosenXAxis]))

  return circlesGroup;
}

// function used for updating circles group with new tooltip
function updateToolTip(chosenXAxis, circlesGroup) {

  var toolTip = d3.tip()
    .attr("class", "tooltip")
    .offset([80, -60])
    .html(function(d) {
      return (`State: ${d.state}<br>${chosenYAxisLabel}: ${d[chosenYAxis]}<br>${chosenXAxisLabel}: ${d[chosenXAxis]}`);
    });

  circlesGroup.call(toolTip);

  circlesGroup.on("mouseover", function(data) {
    toolTip.show(data);
  })
    // onmouseout event
    .on("mouseout", function(data, index) {
      toolTip.hide(data);
    });

  return circlesGroup;
}

// Retrieve data from the CSV file and execute everything below
d3.csv("data/data.csv").then(function(data, err) {
    console.log(data);
  if (err) throw err;

  // xLinearScale function above csv import
  var xLinearScale = xScale(data, chosenXAxis);

  // Create y scale function
  var yLinearScale = d3.scaleLinear()
    .domain([0, d3.max(data, d => +d[chosenYAxis])])
    .range([height, 0]);

  // Create initial axis functions
  var bottomAxis = d3.axisBottom(xLinearScale);
  var leftAxis = d3.axisLeft(yLinearScale);

  // append x axis
  var xAxis = chartGroup.append("g")
    .classed("x-axis", true)
    .attr("transform", `translate(0, ${height})`)
    .call(bottomAxis);

  // append y axis
  chartGroup.append("g")
    .call(leftAxis);

  // append initial circles
  var circlesGroup = chartGroup.selectAll("circle")
    .data(data)
    .enter()
    .append("circle")
    .attr("cx", d => {
        return xLinearScale(+d[chosenXAxis])
    })
    .attr("cy", d => {
        return yLinearScale(+d[chosenYAxis])
    })
    .attr("r", 20)
    .attr("fill", "pink")
    .attr("opacity", ".5");

  chartGroup.selectAll("text")
    .remove();

  let textGroup = chartGroup.selectAll("text")
    .data(data)
    .enter()
    .append("text")
    .attr("x", d => {
        return xLinearScale(+d[chosenXAxis] - 0.3);
    })
    .attr("y", d => {
        return yLinearScale(+d[chosenYAxis] - 0.8);
    })
    .text(d => {
        return d.abbr;
    });

  // Create group for two x-axis labels
  var labelsGroup = chartGroup.append("g")
    .attr("transform", `translate(${width / 2}, ${height + 20})`);

  const step = 20;
  const labels = [
      {property: 'obesity', label: 'Obesity'},
      {property: 'poverty', label: 'Poverty'},
      {property: 'healthcare', label: 'Health Care'},
      {property: 'smokes', label: 'Smokes'},
  ].map((o, i) => {
    const label = labelsGroup.append("text")
        .attr("x", 0)
        .attr("y", (i+1) * step)
        .attr("value", o.property) // value to grab for event listener
        .text(o.label);
    return label;
  })

    boldSelected(labels, chosenXAxis);

  // updateToolTip function above csv import
  var circlesGroup = updateToolTip(chosenXAxis, circlesGroup);

  // x axis labels event listener
  labelsGroup.selectAll("text")
    .on("click", function() {
      // get value of selection
      var value = d3.select(this).attr("value");
      if (value !== chosenXAxis) {

        // replaces chosenXAxis with value
        chosenXAxis = value;

        // console.log(chosenXAxis)

        // functions here found above csv import
        // updates x scale for new data
        xLinearScale = xScale(data, chosenXAxis);

        // updates x axis with transition
        xAxis = renderAxes(xLinearScale, xAxis);

        // updates circles with new x values
        circlesGroup = renderCircles(circlesGroup, xLinearScale, chosenXAxis, textGroup);

        // updates tooltips with new info
        circlesGroup = updateToolTip(chosenXAxis, circlesGroup);

        boldSelected(labels, chosenXAxis)
      }
    });
}).catch(function(error) {
  console.log(error);
});

function boldSelected(labels, selected){
    // changes classes to change bold text
    for (let l of labels){
        const prop = l.attr('value');
        if (prop === selected) {
            l.classed("active", true)
             .classed("inactive", false);
        }
        else {
           l.classed("active", false)
            .classed("inactive", true);
        }
    }
}